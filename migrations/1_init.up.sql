create table users (
    id serial primary key,
    chat_id int not null,
    username varchar(100) not null
);

create table tasks (
    id serial primary key,
    user_id integer not null references users(id),
    summary varchar(255) not null ,
    description text,
    state smallint not null,
    planned_at timestamp,
    resolved_at timestamp,
    updated_at timestamp default NOW() not null,
    created_at timestamp default NOW() not null
)