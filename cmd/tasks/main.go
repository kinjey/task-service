package main

import (
	"task-service/internals/app"
	"task-service/internals/config"
	"task-service/internals/storage"
)

func main() {
	cfg := config.NewConfig()

	store := storage.NewStore(cfg.Db)
	taskServer := app.NewTasksServer(store, cfg.ApiServer)
	taskServer.Listen()
}
