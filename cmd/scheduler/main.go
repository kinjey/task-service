package main

import (
	"task-service/internals/app"
	"task-service/internals/config"
	"task-service/internals/storage"
)

func main() {
	cfg := config.NewConfig()

	store := storage.NewStore(cfg.Db)
	scheduler := app.NewScheduler(store)
	scheduler.Run()
}
