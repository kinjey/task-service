package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

type DbConfig struct {
	Host     string
	Port     string
	User     string
	Pass     string
	Database string
}

type ApiServerConfig struct {
	Host string
	Port string
}

type Config struct {
	Db        DbConfig
	ApiServer ApiServerConfig
}

func LoadEnv() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
		panic(err)
	}
}

func NewConfig() *Config {
	LoadEnv()

	return &Config{
		Db: DbConfig{
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
			User:     os.Getenv("DB_USERNAME"),
			Pass:     os.Getenv("DB_PASSWORD"),
			Database: os.Getenv("DB_DATABASE"),
		},
		ApiServer: ApiServerConfig{
			Host: os.Getenv("API_HOST"),
			Port: os.Getenv("API_PORT"),
		},
	}
}
