package storage

import (
	"github.com/jmoiron/sqlx"
	"task-service/internals/domain"
)

type UserRepository struct {
	Db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	userRepository := &UserRepository{
		Db: db,
	}

	return userRepository
}

func (u *UserRepository) FindById(id int64) (*domain.User, error) {
	user := &domain.User{}

	err := u.Db.Get(user, "SELECT * FROM users WHERE id = $1", id)

	if err != nil {
		return nil, err
	}

	return user, nil
}
