package storage

import (
	"github.com/jmoiron/sqlx"
	"task-service/internals/domain"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.42.2 --name ITaskRepository
type ITaskRepository interface {
	GetById(taskId int64) (*domain.Task, error)
	GetList(offset int, limit int) (*[]domain.Task, error)
	GetDayPlannedTasks(day time.Time) (*sqlx.Rows, error)
	Save(task *domain.Task) error
	Delete(taskId int64) error
}

type TaskRepository struct {
	Db *sqlx.DB
}

func NewTaskRepository(db *sqlx.DB) *TaskRepository {
	taskRepository := &TaskRepository{
		Db: db,
	}

	return taskRepository
}

func (t *TaskRepository) GetById(taskId int64) (*domain.Task, error) {
	task := &domain.Task{}

	err := t.Db.Get(task, "SELECT * FROM tasks WHERE id=$1", taskId)

	if err != nil {
		return nil, err
	}

	return task, nil
}

func (t *TaskRepository) GetList(offset int, limit int) (*[]domain.Task, error) {
	tasks := []domain.Task{}

	if limit == 0 {
		limit = 25
	}

	err := t.Db.Select(&tasks, "SELECT * FROM tasks ORDER BY created_at DESC LIMIT $1 OFFSET $2", limit, offset)

	return &tasks, err
}

func (t *TaskRepository) GetDayPlannedTasks(day time.Time) (*sqlx.Rows, error) {
	startOfDay := time.Date(day.Year(), day.Month(), day.Day(), 0, 0, 0, 0, day.Location())
	endOfDay := time.Date(day.Year(), day.Month(), day.Day(), 23, 59, 59, 0, day.Location())

	rows, err := t.Db.Queryx("SELECT * FROM tasks WHERE planned_at >= $1 and planned_at < $2 ORDER BY user_id, created_at DESC;", startOfDay, endOfDay)

	return rows, err
}

func (t *TaskRepository) Save(task *domain.Task) error {
	var err error

	if task.Id > 0 {
		err = t.update(task)
	} else {
		err = t.create(task)
	}

	return err
}

func (t *TaskRepository) update(task *domain.Task) error {
	currentTime := time.Now()

	task.UpdatedAt = &currentTime

	_, err := t.Db.NamedQuery(
		"UPDATE tasks "+
			"SET user_id = :user_id, summary = :summary, description = :description, state = :state, planned_at = :planned_at, updated_at = :updated_at "+
			"WHERE id = :id",
		task,
	)

	return err
}

func (t *TaskRepository) create(task *domain.Task) error {
	currentTime := time.Now()

	task.CreatedAt = &currentTime
	task.UpdatedAt = &currentTime

	res, err := t.Db.NamedQuery(
		"INSERT INTO tasks "+
			"(user_id, summary, description, state, planned_at, updated_at, created_at) "+
			"VALUES (:user_id, :summary, :description, :state, :planned_at, :updated_at, :created_at)"+
			"RETURNING id;",
		task,
	)

	if err != nil {
		return err
	}

	res.Next()

	return res.Scan(&task.Id)
}

func (t *TaskRepository) Delete(taskId int64) error {
	_, err := t.Db.Exec("DELETE FROM tasks WHERE id = $1", taskId)

	return err
}
