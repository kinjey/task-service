package storage

import (
	"fmt"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"log"
	"task-service/internals/config"
)

type Store struct {
	Db             *sqlx.DB
	userRepository *UserRepository
	taskRepository *TaskRepository
}

func NewStore(config config.DbConfig) *Store {
	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		config.User, config.Pass, config.Host, config.Port, config.Database,
	)

	db, err := sqlx.Connect("pgx", connectionString)
	if err != nil {
		log.Fatalln(err)
	}

	err = db.Ping()

	if err != nil {
		log.Fatalln(err)
	}

	return &Store{
		Db: db,
	}
}

func (s *Store) User() *UserRepository {
	if s.userRepository == nil {
		s.userRepository = NewUserRepository(s.Db)
	}

	return s.userRepository
}

func (s *Store) Task() *TaskRepository {
	if s.taskRepository == nil {
		s.taskRepository = NewTaskRepository(s.Db)
	}

	return s.taskRepository
}
