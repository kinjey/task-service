package app

import (
	"fmt"
	"net/http"
	"task-service/internals/config"
	"task-service/internals/handlers"
	"task-service/internals/storage"
)

type TasksServer struct {
	httpServer *http.Server
}

func setupRouter(store *storage.Store) *http.ServeMux {
	serverMux := http.NewServeMux()

	serverMux.Handle("GET /tasks", &handlers.TaskListHandler{Tasks: store.Task()})
	serverMux.Handle("POST /tasks", &handlers.TaskCreateHandler{Tasks: store.Task()})
	serverMux.Handle("PUT /tasks/{taskId}", &handlers.TaskUpdateHandler{Tasks: store.Task()})
	serverMux.Handle("DELETE /tasks/{taskId}", &handlers.TaskDeleteHandler{Tasks: store.Task()})
	serverMux.Handle("POST /tasks/{taskId}/cancel", &handlers.TaskCancelHandler{Tasks: store.Task()})
	serverMux.Handle("POST /tasks/{taskId}/resolve", &handlers.TaskResolveHandler{Tasks: store.Task()})

	return serverMux
}

func NewTasksServer(store *storage.Store, config config.ApiServerConfig) *TasksServer {
	serverMux := setupRouter(store)
	server := &http.Server{Addr: fmt.Sprintf("%s:%s", config.Host, config.Port), Handler: serverMux}

	taskServer := &TasksServer{httpServer: server}

	return taskServer
}

func (t *TasksServer) Listen() {
	t.httpServer.ListenAndServe()
}
