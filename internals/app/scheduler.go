package app

import (
	"fmt"
	"task-service/internals/domain"
	"task-service/internals/storage"
	"time"
)

type Scheduler struct {
	store *storage.Store
}

func NewScheduler(store *storage.Store) *Scheduler {
	return &Scheduler{
		store: store,
	}
}

func (s *Scheduler) Run() {
	s.TaskRemind()
}

func (s *Scheduler) TaskRemind() {
	taskRepo := s.store.Task()

	tasks, _ := taskRepo.GetDayPlannedTasks(time.Now())

	var userTasks []domain.Task

	var lastUserId int64 = 0

	for tasks.Next() {
		var task domain.Task
		err := tasks.StructScan(&task)

		if err != nil {
			fmt.Println(err)
			continue
		}

		if lastUserId != task.UserId {
			if lastUserId > 0 {
				s.sendTaskNotifications(lastUserId, &userTasks)
				userTasks = []domain.Task{}
			}

			lastUserId = task.UserId
		}

		userTasks = append(userTasks, task)
	}

	if len(userTasks) > 0 {
		s.sendTaskNotifications(lastUserId, &userTasks)
	}
}

func (s *Scheduler) sendTaskNotifications(userId int64, tasks *[]domain.Task) {
	fmt.Println("Сегодня у вас запланированы задачи:")

	for _, task := range *tasks {
		fmt.Println(task.Summary)
	}
}
