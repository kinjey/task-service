package domain

type User struct {
	Id       int64
	ChatId   int64
	Username string
}
