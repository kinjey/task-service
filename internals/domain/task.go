package domain

import "time"

const (
	STATE_PLANNED   = 1
	STATE_RESOLVED  = 2
	STATE_CANCELLED = 3
)

type Task struct {
	Id          int64      `db:"id" json:"id,omitempty"`
	UserId      int64      `db:"user_id" json:"user_id,omitempty"`
	Summary     string     `db:"summary" json:"summary,omitempty"`
	Description string     `db:"description" json:"description,omitempty"`
	State       int8       `db:"state" json:"state,omitempty"`
	PlannedAt   *time.Time `db:"planned_at" json:"planned_at,omitempty"`
	ResolvedAt  *time.Time `db:"resolved_at" json:"resolved_at,omitempty"`
	UpdatedAt   *time.Time `db:"updated_at" json:"updated_at,omitempty"`
	CreatedAt   *time.Time `db:"created_at" json:"created_at,omitempty"`
}
