package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"task-service/internals/domain"
	"task-service/internals/storage"
	"time"
)

type TaskListHandler struct {
	Tasks storage.ITaskRepository
}

func (t *TaskListHandler) ServeHTTP(r http.ResponseWriter, req *http.Request) {
	params := req.URL.Query()

	limit := 100
	page := 1

	if params.Has("page") {
		var err error

		page, err = strconv.Atoi(params.Get("page"))

		if err != nil {
			SendJsonResponse(r, ApiErrorResponse{Error: "invalid page value"}, 400)
			return
		}
	}

	if params.Has("limit") {
		var err error

		limit, err = strconv.Atoi(params.Get("limit"))

		if err != nil {
			SendJsonResponse(r, ApiErrorResponse{Error: "invalid limit value"}, 400)
			return
		}
	}

	tasks, err := t.Tasks.GetList(limit*(page-1), limit)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: err.Error()}, 500)
		return
	}

	SendJsonResponse(r, tasks, 200)
}

type TaskCreateRequest struct {
	Summary     string    `json:"summary"`
	Description string    `json:"description"`
	PlannedAt   time.Time `json:"planned_at"`
}

type TaskCreateHandler struct {
	Tasks storage.ITaskRepository
}

func (t *TaskCreateHandler) ServeHTTP(r http.ResponseWriter, req *http.Request) {
	var taskReq domain.Task
	d := json.NewDecoder(req.Body)
	d.Decode(&taskReq)

	if len(taskReq.Summary) == 0 {
		SendJsonResponse(r, ApiErrorResponse{Error: "summary required"}, 400)
		return
	}

	if taskReq.PlannedAt == nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "planned_at required"}, 400)
		return
	}

	if taskReq.UserId == 0 {
		SendJsonResponse(r, ApiErrorResponse{Error: "user_id required"}, 400)
		return
	}

	taskReq.State = domain.STATE_PLANNED

	err := t.Tasks.Save(&taskReq)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: err.Error()}, 500)
		return
	}

	SendJsonResponse(r, taskReq, 200)
}

type TaskUpdateRequest struct {
	Summary     string     `json:"summary"`
	Description string     `json:"description"`
	PlannedAt   *time.Time `json:"planned_at"`
}

type TaskUpdateHandler struct {
	Tasks storage.ITaskRepository
}

func (t *TaskUpdateHandler) ServeHTTP(r http.ResponseWriter, req *http.Request) {
	taskId, err := strconv.ParseInt(req.PathValue("taskId"), 10, 64)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "invalid taskId value"}, 400)
		return
	}

	task, err := t.Tasks.GetById(taskId)

	if err != nil || task == nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "Task not found"}, 404)
		return
	}

	d := json.NewDecoder(req.Body)
	d.Decode(task)

	err = t.Tasks.Save(task)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: err.Error()}, 500)
		return
	}

	SendJsonResponse(r, task, 200)
}

type TaskCancelHandler struct {
	Tasks storage.ITaskRepository
}

func (t *TaskCancelHandler) ServeHTTP(r http.ResponseWriter, req *http.Request) {
	taskId, err := strconv.ParseInt(req.PathValue("taskId"), 10, 64)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "invalid taskId value"}, 400)
		return
	}

	task, err := t.Tasks.GetById(taskId)

	if err != nil || task == nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "Task not found"}, 404)
		return
	}

	if task.State != domain.STATE_PLANNED {
		SendJsonResponse(r, ApiErrorResponse{Error: "Cancellation allowed only in 'Planned' state"}, 400)
		return
	}

	task.State = domain.STATE_CANCELLED

	err = t.Tasks.Save(task)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: err.Error()}, 500)
	}

	SendJsonResponse(r, SuccessResponse{Success: true}, 200)
}

type TaskResolveHandler struct {
	Tasks storage.ITaskRepository
}

func (t *TaskResolveHandler) ServeHTTP(r http.ResponseWriter, req *http.Request) {
	taskId, err := strconv.ParseInt(req.PathValue("taskId"), 10, 64)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "invalid taskId value"}, 400)
		return
	}

	task, err := t.Tasks.GetById(taskId)

	if err != nil || task == nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "Task not found"}, 404)
		return
	}

	if task.State != domain.STATE_PLANNED {
		SendJsonResponse(r, ApiErrorResponse{Error: "Resolving allowed only in 'Planned' state"}, 400)
		return
	}

	currentTime := time.Now()

	task.State = domain.STATE_RESOLVED
	task.ResolvedAt = &currentTime

	err = t.Tasks.Save(task)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: err.Error()}, 500)
	}

	SendJsonResponse(r, SuccessResponse{Success: true}, 200)
}

type TaskDeleteHandler struct {
	Tasks storage.ITaskRepository
}

func (t *TaskDeleteHandler) ServeHTTP(r http.ResponseWriter, req *http.Request) {
	taskId, err := strconv.ParseInt(req.PathValue("taskId"), 10, 64)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "invalid taskId value"}, 400)
		return
	}

	task, err := t.Tasks.GetById(taskId)

	if err != nil || task == nil {
		SendJsonResponse(r, ApiErrorResponse{Error: "Task not found"}, 404)
		return
	}

	err = t.Tasks.Delete(task.Id)

	if err != nil {
		SendJsonResponse(r, ApiErrorResponse{Error: err.Error()}, 500)
		return
	}

	SendJsonResponse(r, SuccessResponse{Success: true}, 200)
}
