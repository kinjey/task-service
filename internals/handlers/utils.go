package handlers

import (
	"encoding/json"
	"net/http"
)

func SendJsonResponse(r http.ResponseWriter, data interface{}, statusCode int) error {
	body, err := json.Marshal(data)

	if err != nil {
		return err
	}

	r.Header().Set("Content-Type", "application/json")
	r.WriteHeader(statusCode)
	_, err = r.Write(body)

	return err
}
