package handlers

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"task-service/internals/domain"
	"task-service/internals/storage"
	"testing"
)

func TestTaskListHandler(t *testing.T) {
	tests := []struct {
		name        string
		url         string
		want        string
		statusCode  int
		prepareMock func(repository *storage.MockITaskRepository)
	}{
		{
			name:       "empty tasks",
			url:        "/tasks",
			want:       "[]",
			statusCode: http.StatusOK,
			prepareMock: func(mockRepository *storage.MockITaskRepository) {
				mockRepository.On("GetList", mock.Anything, mock.Anything).Return(&[]domain.Task{}, nil)
			},
		},
		{
			name:       "invalid page value",
			url:        "/tasks?page=w",
			want:       "{\"error\":\"invalid page value\"}",
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "invalid limit value",
			url:        "/tasks?limit=w",
			want:       "{\"error\":\"invalid limit value\"}",
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "one task",
			url:        "/tasks",
			want:       "[{\"id\":1,\"summary\":\"Test\",\"state\":1}]",
			statusCode: http.StatusOK,
			prepareMock: func(mockRepository *storage.MockITaskRepository) {
				mockRepository.On("GetList", mock.Anything, mock.Anything).Return(&[]domain.Task{
					{
						Id:      1,
						Summary: "Test",
						State:   1,
					},
				}, nil)
			},
		},
		{
			name:       "pagination (limit=1, page=2)",
			url:        "/tasks?limit=1&page=2",
			want:       "[{\"id\":2,\"summary\":\"Test\",\"state\":2}]",
			statusCode: http.StatusOK,
			prepareMock: func(mockRepository *storage.MockITaskRepository) {
				mockRepository.On("GetList", 1, 1).Return(&[]domain.Task{
					{
						Id:      2,
						Summary: "Test",
						State:   2,
					},
				}, nil)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			tasks := storage.NewMockITaskRepository(t)

			if test.prepareMock != nil {
				test.prepareMock(tasks)
			}

			handler := TaskListHandler{Tasks: tasks}

			req := httptest.NewRequest(http.MethodGet, test.url, nil)
			w := httptest.NewRecorder()

			handler.ServeHTTP(w, req)

			response := w.Result()

			body, err := io.ReadAll(response.Body)

			if err != nil {
				panic(err)
			}

			assert.Equal(t, test.statusCode, response.StatusCode)
			assert.Equal(t, test.want, string(body))

		})
	}
}

func TestTaskCreateHandler(t *testing.T) {
	tests := []struct {
		name        string
		body        string
		want        string
		statusCode  int
		prepareMock func(repository *storage.MockITaskRepository)
	}{
		{
			name:       "summary required",
			body:       "{\"user_id\": 1, \"planned_at\": \"2021-02sw-18T21:54:42.123Z\"}",
			want:       "{\"error\":\"summary required\"}",
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "user_id required",
			body:       "{\"summary\": \"Test\", \"planned_at\": \"2021-02sw-18T21:54:42.123Z\"}",
			want:       "{\"error\":\"user_id required\"}",
			statusCode: http.StatusBadRequest,
		},
		{
			name: "planned_at required",
			body: "{\"summary\": \"Test\", \"user_id\": 1}",

			want:       "{\"error\":\"planned_at required\"}",
			statusCode: http.StatusBadRequest,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			tasks := storage.NewMockITaskRepository(t)

			if test.prepareMock != nil {
				test.prepareMock(tasks)
			}

			handler := TaskCreateHandler{Tasks: tasks}

			req := httptest.NewRequest(http.MethodPost, "/tasks", strings.NewReader(test.body))
			w := httptest.NewRecorder()

			handler.ServeHTTP(w, req)

			response := w.Result()

			body, err := io.ReadAll(response.Body)

			if err != nil {
				panic(err)
			}

			assert.Equal(t, test.statusCode, response.StatusCode)
			assert.Equal(t, test.want, string(body))
		})
	}
}
